import fs from 'fs';

// eslint-disable-next-line no-undef
export default defineNuxtConfig({
  target: 'static',

  /*
   ** Headers of the page
   */
  app: {
    head: {
      title: 'Eine Welt Laden - Bad Neustadt an der Saale',
      meta: [
        { charset: 'utf-8' },
        { name: 'viewport', content: 'width=device-width, initial-scale=1' },
        {
          hid: 'description',
          name: 'description',
          content: 'Eine Welt Laden - Bad Neustadt an der Saale',
        },
        {
          name: 'keywords',
          content:
          'Kaffee, Tee, Kakao, Gewürze, Schokolade, Bananen, bio, fair, fairtrade, fairer Handel, Kunsthandwerk, Taschen, Schmuck, Geschenke, Ehrenamt, ehrenamtlich',
        },
      ],
      link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.png' }],
    },
  },

  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#f7901e' },

  /*
   ** Global CSS
   */
  css: ['~/assets/css/fonts.scss'],

  /*
   ** Nuxt.js modules
   */
  modules: [
    '@nuxtjs/eslint-module',
  ],

  /*
   ** Runtime config
   */
  runtimeConfig: {
    public: {
      openings: fs.readFileSync('./assets/oeffnungszeiten.txt', { encoding: 'utf-8', flag: 'r' }),
    },
  },

  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend (config, ctx) {},
  },
});
