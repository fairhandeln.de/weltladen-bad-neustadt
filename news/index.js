import Jubilaeumsfeier from './jubilaeumsfeier';
import WirSindGemeinwohlzertifiziert from './wir-sind-gemeinwohlzertifiziert';
import NewsEineWeltBildungsstation from './eine-welt-bildungsstation';
import NewsWirSuchenUnterstuetzung from './wir-suchen-unterstuetzung';
import NewsOnlineFairEinkaufen from './online-fair-einkaufen';
import FaireWoche from './faire-woche';
import GwoeZertifiziert from './gwoe-zertifiziert';
import TatorInstallation from './tatort-installation';
import GwoeHintermDeichWirdAllesGut from './gwoe-hinterm-deich-wird-alles-gut';
import ZukunftMitKlasse2023 from './zukunft-mit-klasse-2023';
import KlasseMitZukunftZertifikatGsHerschfeld from './klasse-mit-zukunft-zertifikat-gs-herschfeld';
import DankeFuerDenRegen from './202409DankeFuerDenRegen';

export default {
  DankeFuerDenRegen,
  KlasseMitZukunftZertifikatGsHerschfeld,
  GwoeHintermDeichWirdAllesGut,
  ZukunftMitKlasse2023,
  TatorInstallation,
  GwoeZertifiziert,
  FaireWoche,
  Jubilaeumsfeier,
  WirSindGemeinwohlzertifiziert,
  NewsEineWeltBildungsstation,
  NewsWirSuchenUnterstuetzung,
  NewsOnlineFairEinkaufen,
};
